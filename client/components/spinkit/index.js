//----------------------------------------------------------------------------------------------------------------------
// SpinKit
//
// @module
//----------------------------------------------------------------------------------------------------------------------

import chasingDots from './chasingDots.vue';
import circle from './circle.vue';
import cubeGrid from './cubeGrid.vue';
import doubleBounce from './doubleBounce.vue';
import fadingCircle from './fadingCircle.vue';
import foldingCube from './foldingCube.vue';
import rotatingPlane from './rotatingPlane.vue';
import spinnerPulse from './spinnerPulse.vue';
import threeBounce from './threeBounce.vue';
import wanderingCubes from './wanderingCubes.vue';
import wave from './wave.vue';

//----------------------------------------------------------------------------------------------------------------------

export default {
    chasingDots,
    circle,
    cubeGrid,
    doubleBounce,
    fadingCircle,
    foldingCube,
    rotatingPlane,
    spinnerPulse,
    threeBounce,
    wanderingCubes,
    wave
};

//----------------------------------------------------------------------------------------------------------------------
