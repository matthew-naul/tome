//----------------------------------------------------------------------------------------------------------------------
// Knex Migration configuration
//----------------------------------------------------------------------------------------------------------------------

const dbMan = require('./server/database');

//----------------------------------------------------------------------------------------------------------------------

module.exports = dbMan.getConnObj();

//----------------------------------------------------------------------------------------------------------------------
